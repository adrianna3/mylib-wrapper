import './App.css';
import { Thing} from "mylib3";

function App({domElement}) {
  console.log('domElement',domElement)
  const label = domElement.getAttribute("data-customprop")

  return (
    <div className="App">
      <header className="App-header">

        <Thing label={label} />

      </header>
    </div>
  );
}

export default App;
